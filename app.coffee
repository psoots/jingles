express       = require "express"
app           = express()
server        = require("http").createServer(app)
debug         = require('debug')('jingles')
io            = require("socket.io").listen(server)
path          = require "path"
favicon       = require "serve-favicon"
logger        = require "morgan"
cookieParser  = require "cookie-parser"
bodyParser    = require "body-parser"



# view engine setup
app.set "views", path.join(__dirname, "app/views")
app.set "view engine", "hjs"


app.use logger "dev"
app.use bodyParser.json()
app.use bodyParser.urlencoded extended: false
app.use cookieParser()
app.use express.static path.join(__dirname, "app/public")

require("./app/routes")(app, io)

# catch 404 and forward to error handler
app.use (req, res, next) ->
    err = new Error "Not Found"
    err.status = 404
    next err
    return

# error handlers

# development error handler
# will print stacktrace
if app.get("env") is "development"
    app.use (err, req, res, next) ->
        res.status err.status or 500
        res.render "error",
            message: err.message
            error: err


# production error handler
# no stacktraces leaked to user
app.use (err, req, res, next) ->
    res.status err.status or 500
    res.render "error",
        message: err.message
        error: {}

app.set 'port', process.env.PORT or 3000

server.listen app.get('port'), ->
  debug 'Express server listening on port ' + server.address().port