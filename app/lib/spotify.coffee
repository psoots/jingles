SpotifyWebApi = require 'spotify-web-api-node'
Queue         = require './queue'
Events        = require './events'
mac           = require 'applescript'
debug         = require('debug')('jingles')
Promise       = require 'promise'

class Spotify

  api = new SpotifyWebApi()
  playlistName = "jingles#{Date.now()}"
  playlistID = null
  currentTimeout = null

  auth =
    creds:
      clientId: 'd7087d4659d14c088d719c043835060c'
      clientSecret: 'edf2fb9c18744239ba530b631366d2a6'
      redirectUri: 'http://localhost:3000/callback'
    scopes: ['playlist-read-private','playlist-modify-private','playlist-modify-public']
    state: 'jingles-spotify-auth'
    accessToken: null
    refreshToken: null
    expiresIn: null
    code: null


  userID = 123604606
  defaultPlaylistIDs = ['21irRBkipdtfYpWcgtpyKQ', '4kVItatxgG4kzzlxM3ijEH']

  scripts =
    play: (uri) -> "tell application \"Spotify\" to play track \"#{uri}\""
    activate: "tell application \"Spotify\" to activate"


  @isReady: -> auth.code?

  @authenticate: (res) ->
    api.setCredentials auth.creds
    res.redirect api.createAuthorizeURL auth.scopes, auth.state

  @authenticated: (req) ->
    auth.code = req.query.code
    api.authorizationCodeGrant(auth.code)
      .then(
        (data) ->
          auth.accessToken = data.access_token
          auth.refreshToken = data.refresh_token
          auth.expiresIn = data.expires_in * 1000
          api.setAccessToken auth.accessToken
          api.setRefreshToken auth.refreshToken
          setTimeout refreshToken, auth.expiresIn - 5000
          start()
      , ohNo
      )

  # Tracks go to the front of the line
  @addTrack: (uri) ->
    id = uri.replace 'spotify:track:', '', uri
    api.getTrack(id).then (track) ->
      Queue.playNext track
      Events.trigger 'playlist:new-next', Queue.listFirstTracks()


  # Skip the currently playing track
  @skip: ->
    clearTimeout currentTimeout
    playNext()

  @search: (query) ->
    api.searchTracks(query, limit: 30, market: 'US').then (results) ->
      Events.trigger 'spotify:search:completed', results.tracks.items
    , ohNo

  ohNo = (err) ->
    console.error err
    process.exit(1)

  refreshToken = ->
    api.refreshAccessToken().then (data) ->
      debug "Refreshed token"
      fiveMins = 300000
      twentyMins = 20 * 60 * 1000
      setTimeout refreshToken, Math.max(Math.abs(auth.expiresIn - fiveMins), twentyMins)
    , ohNo

  playNext = (notifyEvent = 'playlist:updated') ->
    track = Queue.pop()
    uri = track.uri
    api.addTracksToPlaylist(userID, playlistID, [uri])
    mac.execString scripts.play(uri), (err, rtn) ->
      Events.trigger notifyEvent, Queue.listFirstTracks()
      return ohNo err if err
      currentTimeout = setTimeout playNext, track.duration_ms

  start = ->

    getDefaultTracks = (newPlaylistData) ->
      playlistID = newPlaylistData.id
      promises = []
      for defaultPlaylistID in defaultPlaylistIDs
        promises.push api.getPlaylistTracks userID, defaultPlaylistID
        promises.push api.getPlaylistTracks userID, defaultPlaylistID, {offset: 100} # so, we're getting the first 200 tracks
      return Promise.all(promises)

    fillNewPlaylist = (playlists) ->
      for playlistTracks in playlists
        for item in playlistTracks.items
          Queue.addTrack item.track
      mac.execString scripts.activate, (err, rtn) ->
        return ohNo err  if err
        playNext()

    api.createPlaylist(userID, playlistName)
      .then(getDefaultTracks, ohNo)
      .then(fillNewPlaylist, ohNo)


module.exports = Spotify
