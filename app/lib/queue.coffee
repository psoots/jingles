Events    = require './events'
shuffle   = require 'mout/array/shuffle'

class Queue

  allTracks = {}
  allTrackIDs = []

  minimumQueueLength = 10
  queue = []
  priorityQueue = []
  currentTrack = null

  arrFromObjKeys = (obj, keys) -> obj[key] for key in keys

  # A pass through function to ensure the queue has some elements
  prep = (cb) ->
    if priorityQueue.length + queue.length < minimumQueueLength
      queue.push.apply queue, shuffle allTrackIDs
    return cb()

  @pop: -> prep ->
    trackID = priorityQueue.pop()
    return currentTrack = allTracks[trackID] if trackID
    return currentTrack = allTracks[queue.pop()]

  @listFirstTracks: (limit = 10) ->
    IDs = prep ->
      q = queue.concat priorityQueue
      return q.slice(-1 * limit)
    IDs.reverse()
    return [currentTrack].concat arrFromObjKeys allTracks, IDs

  @playNext: (track) ->

    # Add the track to our inventory if it doesn't exist
    @addTrack track

    # If the ID is queued in normal queue, remove it
    # If it's in the priority queue, leave that as it is
    key = queue.indexOf track.id
    queue.splice key, 1 unless key == -1

    # Add to the end of the priorityQueue only
    priorityQueue = [track.id].concat priorityQueue


  @addTrack: (track) ->
    trackID = track.id
    unless allTracks[trackID]
      allTracks[trackID] = track
      allTrackIDs.push trackID





module.exports = Queue