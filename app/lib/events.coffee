EventEmitter = require('events').EventEmitter

class Events

  # Called once here
  events = new EventEmitter()

  @on: (event, listener) -> events.on event, listener
  @trigger: (event, data) -> events.emit event, data


module.exports = Events
