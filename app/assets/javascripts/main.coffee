class Dispatcher

  socket = io()


  keymap =
    default:
      32: 'openSearch'      # space
      83: 'skip'            # s
      39: 'skip'            # right arrow
    search:
      27: 'closeSearch'      # esc
    results:
      27: 'closeSearch'     # esc
      13: 'addSong'         # enter
      38: 'seekUp'          # up
      40: 'seekDown'        # down



  constructor: ->
    @state = 'default'
    @prepareQueue()
    @$search = $ '#search'
    @$searchResult = $ '#search-result'
    @$searchInput = @$search.find 'input'
    @$searchForm = @$search.find 'form'
    @$searchingMsg = $ '#searching-msg'
    @$imagesArray = $ '#images-array'
    @$songInfo = @$imagesArray.find('.song-info')
    @$currentArtist = @$songInfo.find('h2')
    @$currentTrack = @$songInfo.find('h4')
    @$keyboardImg = $ '#keyboard img'
    @$flash = $ '#flash'

    $(window).keyup (event) =>

      map = keymap[@state]
      key = event.keyCode
      if map[key]
        event.preventDefault()
        @[map[key]].call @

  openSearch: ->
    @state = 'search'
    @updateKeyboard()
    @$search.show()
    @$searchInput.show()
    @$searchingMsg.hide()
    @$searchInput.focus()
    @$searchForm.submit (event) =>
      event.preventDefault()
      query = $.trim @$searchInput.val()
      return @$searchInput.focus() if query == ''
      socket.emit 'search', query
      @$searchInput.hide()
      @$searchingMsg.show()

    socket.on 'search:completed', @showResults.bind @

  closeSearch: ->
    @state = 'default'
    @updateKeyboard()
    $('#search').hide()
    @$searchInput.val ''
    @$imagesArray.find('.album-cover').slice(0, 3).removeClass 'placeholder'
    @$searchResult.hide()


  addSong: ->
    uri = @$searchResult.find('li.active').data('track-uri')
    socket.emit 'addTrack', uri
    @closeSearch()
    msgs = [
      "Your track is next in line, Bucko"
      "It's like a jukebox, you gotta wait to hear your song"
      "okie dokie, your song is in the queue"
      "Woah, great song. So happy you picked that."
      "Congratulations! You picked the magic song. Go see Zach about your present."
      "Thank you! What is xmas without this song?"
    ]

    @$flash.text msgs[Math.floor(Math.random() * msgs.length)]
    setTimeout =>
      @$flash.fadeOut()
    , 5000

  skip: -> socket.emit 'skip'

  seekDown: ->
    $x = @$searchResult.find('li.active')
    $next = $x.next('li')
    index = @$searchResult.find('li').index($next)
    if $next.length
      $x.removeClass('active')
      $next.addClass('active')
      @$searchResult.animate {scrollTop: @$searchResult.scrollTop() + 80}, 100

  seekUp: ->
    $x = @$searchResult.find('li.active')
    $prev = $x.prev('li')
    index =  @$searchResult.find('li').index($prev)
    if $prev.length
      $x.removeClass('active')
      $prev.addClass('active')
      @$searchResult.animate {scrollTop: @$searchResult.scrollTop() - 80}, 100

  showResults: (results) ->
    @closeSearch()
    @state = 'results'
    @updateKeyboard()
    @$imagesArray.find('.album-cover').slice(0, 3).addClass 'placeholder'
    @$searchResult.animate {scrollTop: 0}, 10
    $ul = @$searchResult.find 'ul'
    $ul.html ''
    $ul.focus()
    for track in results
      trackName = track.name
      artist = track.artists[0].name
      albumName = track.album.name
      uri = track.uri
      $li =
        """
        <li data-track-uri="#{uri}">
          <h3 class="result-track">#{trackName} <span class="result-by">by</span> <span class="result-artist">#{artist}</span></h3>
          <h5 class="result-album">on the "#{albumName}" album</h5>
        </li>
        """
      $ul.append $li
      $ul.find('li').first().addClass 'active'

    @$searchResult.show()

  updateKeyboard: ->
    state = @state.charAt(0).toUpperCase()+@state.slice(1)
    @$keyboardImg.attr 'src', "images/keyboard#{state}.png"

  prepareQueue: ->

    $getImg = (index) => @$imagesArray.find(".album-cover:nth-child(#{index}) img")

    slideImages = ->
      for i in [1..4]
        $getImg(i).attr 'src', $getImg(i+1).attr 'src'

    setNewCovers = (playlist) ->
      i = 4
      for track in playlist
        break if i > 9
        $getImg(i).attr 'src', track.album.images[0].url
        ++i

    socket.on 'playlist:updated', (data) =>
      slideImages()
      setNewCovers data
      currentTrack = data[0]
      artist = currentTrack.artists[0].name
      album = currentTrack.album.name
      trackName = currentTrack.name
      @$currentArtist.text artist
      @$currentTrack.text trackName

    socket.on 'playlist:new-next', setNewCovers

$ -> new Dispatcher()