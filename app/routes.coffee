Spotify = require './lib/spotify'
Events = require './lib/events'

module.exports = (app, io) ->

  app.get '/', (req, res) ->
    if Spotify.isReady()
      res.render 'index'
    else
      res.redirect '/login'

  io.on 'connection', (socket) ->
    # Client-side api
    socket.on 'addTrack', (url) -> Spotify.addTrack url
    socket.on 'skip',           -> Spotify.skip()
    socket.on 'search', (query) -> Spotify.search query

  app.get '/login', (req, res) ->
    Spotify.authenticate res

  app.get '/callback', (req, res) ->
    Spotify.authenticated req
    if Spotify.isReady()
      res.redirect '/'
    else
      res.render 'error'

  Events.on 'spotify:search:completed', (results) ->
    io.emit 'search:completed', results

  Events.on 'playlist:updated', (playlist) ->
    io.emit 'playlist:updated', playlist

  Events.on 'playlist:new-next', (playlist) ->
    io.emit 'playlist:new-next', playlist
