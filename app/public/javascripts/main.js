(function() {
  var Dispatcher;

  Dispatcher = (function() {
    var keymap, socket;

    socket = io();

    keymap = {
      "default": {
        32: 'openSearch',
        83: 'skip',
        39: 'skip'
      },
      search: {
        27: 'closeSearch'
      },
      results: {
        27: 'closeSearch',
        13: 'addSong',
        38: 'seekUp',
        40: 'seekDown'
      }
    };

    function Dispatcher() {
      this.state = 'default';
      this.prepareQueue();
      this.$search = $('#search');
      this.$searchResult = $('#search-result');
      this.$searchInput = this.$search.find('input');
      this.$searchForm = this.$search.find('form');
      this.$searchingMsg = $('#searching-msg');
      this.$imagesArray = $('#images-array');
      this.$songInfo = this.$imagesArray.find('.song-info');
      this.$currentArtist = this.$songInfo.find('h2');
      this.$currentTrack = this.$songInfo.find('h4');
      this.$keyboardImg = $('#keyboard img');
      this.$flash = $('#flash');
      $(window).keyup((function(_this) {
        return function(event) {
          var key, map;
          map = keymap[_this.state];
          key = event.keyCode;
          if (map[key]) {
            event.preventDefault();
            return _this[map[key]].call(_this);
          }
        };
      })(this));
    }

    Dispatcher.prototype.openSearch = function() {
      this.state = 'search';
      this.updateKeyboard();
      this.$search.show();
      this.$searchInput.show();
      this.$searchingMsg.hide();
      this.$searchInput.focus();
      this.$searchForm.submit((function(_this) {
        return function(event) {
          var query;
          event.preventDefault();
          query = $.trim(_this.$searchInput.val());
          if (query === '') {
            return _this.$searchInput.focus();
          }
          socket.emit('search', query);
          _this.$searchInput.hide();
          return _this.$searchingMsg.show();
        };
      })(this));
      return socket.on('search:completed', this.showResults.bind(this));
    };

    Dispatcher.prototype.closeSearch = function() {
      this.state = 'default';
      this.updateKeyboard();
      $('#search').hide();
      this.$searchInput.val('');
      this.$imagesArray.find('.album-cover').slice(0, 3).removeClass('placeholder');
      return this.$searchResult.hide();
    };

    Dispatcher.prototype.addSong = function() {
      var msgs, uri;
      uri = this.$searchResult.find('li.active').data('track-uri');
      socket.emit('addTrack', uri);
      this.closeSearch();
      msgs = ["Your track is next in line, Bucko", "It's like a jukebox, you gotta wait to hear your song", "okie dokie, your song is in the queue", "Woah, great song. So happy you picked that.", "Congratulations! You picked the magic song. Go see Zach about your present.", "Thank you! What is xmas without this song?"];
      this.$flash.text(msgs[Math.floor(Math.random() * msgs.length)]);
      return setTimeout((function(_this) {
        return function() {
          return _this.$flash.fadeOut();
        };
      })(this), 5000);
    };

    Dispatcher.prototype.skip = function() {
      return socket.emit('skip');
    };

    Dispatcher.prototype.seekDown = function() {
      var $next, $x, index;
      $x = this.$searchResult.find('li.active');
      $next = $x.next('li');
      index = this.$searchResult.find('li').index($next);
      if ($next.length) {
        $x.removeClass('active');
        $next.addClass('active');
        return this.$searchResult.animate({
          scrollTop: this.$searchResult.scrollTop() + 80
        }, 100);
      }
    };

    Dispatcher.prototype.seekUp = function() {
      var $prev, $x, index;
      $x = this.$searchResult.find('li.active');
      $prev = $x.prev('li');
      index = this.$searchResult.find('li').index($prev);
      if ($prev.length) {
        $x.removeClass('active');
        $prev.addClass('active');
        return this.$searchResult.animate({
          scrollTop: this.$searchResult.scrollTop() - 80
        }, 100);
      }
    };

    Dispatcher.prototype.showResults = function(results) {
      var $li, $ul, albumName, artist, track, trackName, uri, _i, _len;
      this.closeSearch();
      this.state = 'results';
      this.updateKeyboard();
      this.$imagesArray.find('.album-cover').slice(0, 3).addClass('placeholder');
      this.$searchResult.animate({
        scrollTop: 0
      }, 10);
      $ul = this.$searchResult.find('ul');
      $ul.html('');
      $ul.focus();
      for (_i = 0, _len = results.length; _i < _len; _i++) {
        track = results[_i];
        trackName = track.name;
        artist = track.artists[0].name;
        albumName = track.album.name;
        uri = track.uri;
        $li = "<li data-track-uri=\"" + uri + "\">\n  <h3 class=\"result-track\">" + trackName + " <span class=\"result-by\">by</span> <span class=\"result-artist\">" + artist + "</span></h3>\n  <h5 class=\"result-album\">on the \"" + albumName + "\" album</h5>\n</li>";
        $ul.append($li);
        $ul.find('li').first().addClass('active');
      }
      return this.$searchResult.show();
    };

    Dispatcher.prototype.updateKeyboard = function() {
      var state;
      state = this.state.charAt(0).toUpperCase() + this.state.slice(1);
      return this.$keyboardImg.attr('src', "images/keyboard" + state + ".png");
    };

    Dispatcher.prototype.prepareQueue = function() {
      var $getImg, setNewCovers, slideImages;
      $getImg = (function(_this) {
        return function(index) {
          return _this.$imagesArray.find(".album-cover:nth-child(" + index + ") img");
        };
      })(this);
      slideImages = function() {
        var i, _i, _results;
        _results = [];
        for (i = _i = 1; _i <= 4; i = ++_i) {
          _results.push($getImg(i).attr('src', $getImg(i + 1).attr('src')));
        }
        return _results;
      };
      setNewCovers = function(playlist) {
        var i, track, _i, _len, _results;
        i = 4;
        _results = [];
        for (_i = 0, _len = playlist.length; _i < _len; _i++) {
          track = playlist[_i];
          if (i > 9) {
            break;
          }
          $getImg(i).attr('src', track.album.images[0].url);
          _results.push(++i);
        }
        return _results;
      };
      socket.on('playlist:updated', (function(_this) {
        return function(data) {
          var album, artist, currentTrack, trackName;
          slideImages();
          setNewCovers(data);
          currentTrack = data[0];
          artist = currentTrack.artists[0].name;
          album = currentTrack.album.name;
          trackName = currentTrack.name;
          _this.$currentArtist.text(artist);
          return _this.$currentTrack.text(trackName);
        };
      })(this));
      return socket.on('playlist:new-next', setNewCovers);
    };

    return Dispatcher;

  })();

  $(function() {
    return new Dispatcher();
  });

}).call(this);
