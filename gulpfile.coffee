gulp    = require 'gulp'
sass    = require 'gulp-sass'
coffee  = require 'gulp-coffee'
gutil   = require 'gulp-util'



gulp.task 'sass', ->
  gulp.src('app/assets/stylesheets/style.scss')
    .pipe(sass())
    .pipe(gulp.dest('app/public/stylesheets/'))

gulp.task 'coffee', ->
  gulp.src('app/assets/javascripts/main.coffee')
    .pipe(coffee().on('error', gutil.log))
    .pipe(gulp.dest('app/public/javascripts/'))

gulp.task 'watch', ->
  gulp.watch 'app/assets/javascripts/**/*.coffee', ['coffee']
  gulp.watch 'app/assets/stylesheets/**/*.scss', ['sass']

gulp.task 'default', ['coffee', 'sass', 'watch']
